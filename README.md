[![pipeline status](https://gitlab.com/b1g_J/arching_some_comps/badges/master/pipeline.svg)](https://gitlab.com/b1g_J/arching_some_comps/commits/master)

# arching some comps
Because I'm not going to install verilog on my system. 

### Starting the container
Go ahead and pull the image from the docker registry with `docker-compose pull`. Then to start the container, you can use the run.sh script with `./run.sh`. This will give you a zsh shell in the container.

### Compiling verilog
You can compile verilog from within the container with `iverilog [file]`. You can open verilog dump files with `gtkwave [file]`.

# Maintainer
- big_J
