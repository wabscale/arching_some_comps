FROM debian:stretch
MAINTAINER big_J

RUN apt update
RUN apt install verilog gtkwave -y
RUN apt install emacs zsh -y

WORKDIR /root

#CMD gtkwav
CMD zsh
